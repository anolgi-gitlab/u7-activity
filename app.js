const express = require("express");
const app = express();

app.get("/", (request, response) => {
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.end("Hello world with nodejs. Avui toca mirar Los Goya!! ... o no");
});

const PORT = process.env.PORT || 5000;
app.listen(PORT);
